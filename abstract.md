# Structure and State

Immutable data structures and an emphasis on avoiding side-effects in
functional programming can lead to challenges when managing application
state. This talk explores a way to structure an application such that it
does contain no global state in `def`s or similar constructs. At the
same time the presented structure allows to easily replace all of the
application's components by different implementations. Among other
things, this provides the added benefit of being able to mock parts of
the application without resorting to monkey-patching functions.
