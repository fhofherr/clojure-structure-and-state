(ns hello-service.adapters
  (:require [hello-service.adapters
             [config :as config]
             [persistence :as persistence]
             [http :as http]]))

(defn init
  [system]
  (-> system
      config/init
      persistence/init
      http/init))

(defn shutdown
  [system]
  (-> system
      http/shutdown
      persistence/shutdown
      config/shutdown))
