(ns hello-service.core.users
  "Contains the core business logic to manage users."
  (:require [clojure.spec.alpha :as s]
            [hello-service.core
             [uuid :as uuid]
             [errors :as errors]]))

(s/def :hello-service.core/user-name (and string? (complement empty?)))
(s/def :hello-service.core/password (and string? (complement empty?)))
(s/def :hello-service.core/user-id uuid/uuid?)

(s/def :hello-service.core/new-user (s/keys :req-un [:hello-service.core/user-name
                                                     :hello-service.core/password]))
(s/def :hello-service.core/user (s/merge :hello-service.core/new-user
                                         (s/keys :req-un [:hello-service.core/user-id])))

(defprotocol UserRepository
  "UserRepository keeps track of users."

  (-get-user [this user-id]
             "Get an user by their user-id, or nil if no
             user for the given user-id exists.")

  (-put-user [this user]
             "Save an user. If saving an user to the repository was
             not possible, -put-user may throw an ex-info with
             one of the causes listed in [[errors]]."))

(defn add-user
  "Add a new user. The new user must be valid according to the spec for new
  users."
  [{:hello-service.core/keys [user-repository]} new-user]
  (when-not (s/valid? :hello-service.core/new-user new-user)
    (throw (ex-info "Validation failed"
                    (errors/validation-failed :hello-service.core/new-user new-user))))
  (let [user (assoc new-user :user-id (uuid/random-uuid))]
    (-put-user user-repository user)
    user))

(defn get-user
  "Get the user identified by user-id."
  [{:hello-service.core/keys [user-repository]} user-id]
  (-get-user user-repository user-id))
