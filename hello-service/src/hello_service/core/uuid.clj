(ns hello-service.core.uuid
  (:refer-clojure :exclude [uuid?]))

(defn uuid?
  [o]
  (instance? java.util.UUID o))

(defn random-uuid
  []
  (java.util.UUID/randomUUID))

(defn from-string
  [o]
  (cond
    (uuid? o) o
    (string? o) (java.util.UUID/fromString o)
    :else (throw (IllegalArgumentException.
                   (format "Could not parse '%s' into an UUID" o)))))
