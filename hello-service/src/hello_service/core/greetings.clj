(ns hello-service.core.greetings
  (:require [hello-service.core
             [errors :as errors]
             [uuid :as uuid]
             [users :as users]]))

(defprotocol GreetingRepository
  (-get-greeting [this user-id] "Get a custom greeting for user-id")
  (-put-greeting [this user greeting]))

(defn greet-user
  "Generate a greeting for the user with the given user-id"
  [{:hello-service.core/keys [greeting-repository] :as core} user-id]
  (when-let [{user-name :user-name} (users/get-user core user-id)]
    (if-let [{g :greeting} (-get-greeting greeting-repository user-id)]
      {:greeting-message (format "%s, %s" g user-name)}
      {:greeting-message (format "Hello, %s" user-name)})))

(defn customize-greeting
  [{:hello-service.core/keys [greeting-repository] :as core}
   user-id
   new-greeting]
  (if-let [user (users/get-user core user-id)]
    (let [greeting (assoc new-greeting :greeting-id (uuid/random-uuid))]
      (-put-greeting greeting-repository user greeting)
      greeting)
    (throw (ex-info "No such user"
                    {:cause ::errors/no-such-user
                     :detail {:user-id user-id}}))))
