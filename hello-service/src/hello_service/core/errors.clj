(ns hello-service.core.errors
  (:require [clojure.spec.alpha :as s]))

(defn validation-failed
  [spec o]
  {:cause ::validation-failed
   :detail (s/explain-data spec o)})
