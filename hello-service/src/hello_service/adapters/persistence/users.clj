(ns hello-service.adapters.persistence.users
  (:refer-clojure :exclude [update])
  (:require [clojure.spec.alpha :as s]
            [camel-snake-kebab.core :refer [->snake_case ->kebab-case-keyword]]
            [clojure.java.jdbc :as jdbc]
            [honeysql.core :as sql]
            [honeysql.helpers :refer :all]
            [hello-service.core
             [errors :as errors]
             [users :as users]]
            [hello-service.adapters.persistence.db :as db]))

(defn- transform-keys
  [m f]
  (into {} (map (fn [[k v]] [(f k) v]) m)))

(defn- put-user
  [db-spec user]
  (when-not (s/valid? :hello-service.core/user user)
    (throw (ex-info "Validation failed"
                    (errors/validation-failed :hello-service.core/user user))))
  (let [stmt (-> (insert-into :t_users)
                 (values [(transform-keys user ->snake_case)])
                 sql/format)]
    (jdbc/with-db-transaction [t-con db-spec]
      (jdbc/execute! t-con stmt))))

(defn- get-user
  [db-spec user-id]
  (let [stmt (-> (select :user_id :user_name :password)
                 (from :t_users)
                 (where [:= :user_id user-id])
                 sql/format)]
    (jdbc/with-db-transaction [t-con db-spec]
      (jdbc/query t-con stmt {:identifiers ->kebab-case-keyword
                              :result-set-fn first}))))

(defn new-user-repository
  "Create a user repository storing users to a database"
  [db-spec]
  (reify users/UserRepository
    (-put-user [_ user] (put-user db-spec user))
    (-get-user [_ user-id] (get-user db-spec user-id))))
