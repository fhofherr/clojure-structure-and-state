(ns hello-service.adapters.persistence.db
  (:import com.zaxxer.hikari.HikariDataSource
           org.flywaydb.core.Flyway))

(defn- new-hikaricp
  [db-url db-user db-password]
  (doto (HikariDataSource.)
    (.setJdbcUrl db-url)
    (.setUsername db-user)
    (.setPassword db-password)))

(defn- migrate-db
  [ds]
  (let [fw (doto (Flyway.)
             (.setDataSource ds)
             (.setTable "t_flyway_schema_version")
             (.setBaselineOnMigrate true))]
    (.migrate fw)))

(defn new-db-spec
  "Initialize the database."
  [db-url db-user db-password]
  (let [ds (new-hikaricp db-url db-user db-password)]
    (migrate-db ds)
    {:datasource ds}))

(defn close
  "Close the database."
  [db-spec]
  (-> db-spec :datasource (.close)))
