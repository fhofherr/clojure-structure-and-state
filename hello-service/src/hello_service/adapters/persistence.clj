(ns hello-service.adapters.persistence
  (:require [hello-service.adapters.config :as config]
            [hello-service.adapters.persistence
             [db :as db]
             [users :as users]]))

(defn init
  [{:keys [::config/settings] :as system}]
  (let [db-url (config/get-setting settings :db-url)
        db-user (config/get-setting settings :db-user)
        db-password (config/get-setting settings :db-password)
        db-spec (db/new-db-spec db-url db-user db-password)
        user-repo (users/new-user-repository db-spec)]
    (assoc system
           ::db-spec db-spec
           :hello-service.core/user-repository user-repo)))

(defn shutdown
  [{db-spec ::db-spec :as system}]
  (db/close db-spec)
  (dissoc system ::db-spec :hello-service.core/user-repository))
