(ns hello-service.adapters.config
  (:require [environ.core :refer [env]]))

(defn get-setting
  [settings kw & [default]]
  (if-let [v (get settings kw default)]
    v
    (throw (RuntimeException. (format "Missing configuration: %s" kw)))))

(defn init
  [system]
  (assoc system ::settings env))

(defn shutdown
  [system]
  (dissoc system ::settings))
