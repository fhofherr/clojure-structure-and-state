(ns hello-service.adapters.http
  (:require [compojure.core :as compojure]
            [org.httpkit.server :as http-server]
            [ring.middleware.json :as json]
            [hello-service.adapters
             [config :as config]
             [persistence :as persistence]]
            [hello-service.adapters.http
             [greetings :as greetings]
             [users :as users]]))

(defn- new-app-routes
  [system]
  (let [user-routes (users/init system)
        greeting-routes (greetings/init system)]
    (compojure/routes
      (compojure/context "/users" [] user-routes)
      (compojure/context "/greetings" [] greeting-routes))))

(defn- new-handler
  [system]
  (let [routes (new-app-routes system)]
    (-> routes
        (json/wrap-json-body {:keywords? true :big-decimals? true})
        json/wrap-json-response)))

(defn init
  [{:keys [::config/settings] :as system}]
  (let [port (config/get-setting settings :http-port 8080)
        handler (new-handler system)
        server (http-server/run-server handler {:port port})]
    (assoc system ::server server)))

(defn shutdown
  [{server ::server :as system}]
  (server :timeout 100)
  (dissoc system ::server))
