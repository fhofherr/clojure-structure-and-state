(ns hello-service.adapters.http.greetings
  (:require [compojure.core
             :refer [GET POST]
             :as compojure]
            [ring.util.response :as response]
            [hello-service.core
             [uuid :as uuid]
             [greetings :as greetings]]))

(defprotocol GreetingOps
  (-greet-user [this user-id])
  (-customize-greeting [this user-id greeting]))

(defn greet-user
  [ops user-id]
  (let [id (uuid/from-string user-id)
        greeting (-greet-user ops user-id)]
    (-> {:body greeting}
        (response/status 200))))

(defn customize-greeting
  [ops base-url user-id greeting]
  (let [greeting (-customize-greeting ops user-id greeting)
        url (str base-url (:greeting-id greeting))]
    (response/created url greeting)))

(defn new-greeting-routes
  [greeting-ops]
  (compojure/routes
    (GET "/:user-id" [user-id] (greet-user greeting-ops user-id))
    (POST "/:user-id" [user-id :as req] (customize-greeting greeting-ops
                                                            (:uri req)
                                                            user-id
                                                            (:body req)))))

(defn new-core-greeting-ops
  [system]
  (reify GreetingOps
    (-greet-user [_ user-id] (greetings/greet-user system user-id))
    (-customize-greeting [_ user-id greeting]
      (greetings/customize-greeting system user-id greeting))))

(defn init
  [system]
  (-> system new-core-greeting-ops new-greeting-routes))
