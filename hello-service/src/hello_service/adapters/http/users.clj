(ns hello-service.adapters.http.users
  (:require [compojure.core
             :refer [GET POST]
             :as compojure]
            [ring.util.response :as response]
            [hello-service.core
             [uuid :as uuid]
             [users :as users]]))

(defprotocol UserOps
  (-get-user [this user-id])
  (-add-user [this user]))

(defn- add-user
  [ops base-url new-user]
  (let [user (-add-user ops new-user)
        url (str base-url (:user-id user))]
    (response/created url user)))

(defn- get-user
  [ops user-id]
  (let [id (uuid/from-string user-id)
        user (-get-user ops id)]
    (-> {:body user}
        (response/status 200))))

(defn new-user-routes
  [user-ops]
  (compojure/routes
    (POST "/" req (add-user user-ops (:uri req) (:body req)))
    (GET "/:user-id" [user-id] (get-user user-ops user-id))))

(defn new-core-user-ops
  [system]
  (reify UserOps
    (-get-user [_ user-id] (users/get-user system user-id))
    (-add-user [_ user] (users/add-user system user))))

(defn init
  [system]
  (-> system new-core-user-ops new-user-routes))
