(ns hello-service.main
  (:require [hello-service.adapters :as adapters]))

(defn init
  [system]
  (adapters/init system))

(defn shutdown
  [system]
  (adapters/shutdown system))

(defn -main
  [& args]
  (let [system (init {})]
    (-> (Runtime/getRuntime)
        (.addShutdownHook (Thread. #(shutdown system))))
    (println "Server started...")))
