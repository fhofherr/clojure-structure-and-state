(ns hello-service.test.core.users-test
  (:require [clojure.test :refer :all]
            [fhofherr.stub-fn.clojure.test :refer [stub-protocol]]
            [hello-service.core
             [errors :as errors]
             [users :as users]
             [uuid :as uuid]]))

(deftest adding-users

  (testing "add a new user to an user repository"
    (let [user-repo (stub-protocol
                      users/UserRepository
                      (-put-user [this user]))
          new-user {:user-name "John Doe" :password "12345"}
          user (users/add-user {:hello-service.core/user-repository user-repo} new-user)]
      (is (= new-user
             (select-keys user [:user-name :password])))
      (is (invoked? user-repo
                    :method '-put-user
                    :args {'this user-repo
                           'user user}))))

  (testing "adding an invalid user results in an ex-info"
    (let [user-repo (stub-protocol
                      users/UserRepository
                      (-put-user [this user]))
          invalid-user {:password "12345"}]
      ;; We don't use thrown? here, as we want to make further assertions
      ;; about the exception thrown.
      ;; TODO: extend assert-expr to make this possible
      (try
        (users/add-user {:hello-service.core/user-repository user-repo} invalid-user)
        (is false "Expected exception to be thrown")
        (catch clojure.lang.ExceptionInfo e
          (let [{:keys [cause detail]} (ex-data e)]
            (is (= ::errors/validation-failed cause))
            (is (not-empty detail)))
          (is (invoked? user-repo :method '-put-user :times 0)))))))

(deftest getting-users

  (testing "get a non-existent user"
    (let [user-repo (stub-protocol
                      users/UserRepository
                      (-get-user [this user-id]))
          user-id (uuid/random-uuid)]
      (is (nil? (users/get-user {:hello-service.core/user-repository user-repo} user-id)))
      (is (invoked? user-repo :method '-get-user :times 1))))

  (testing "get an existing user"
    (let [user {:user-name "John Doe"
                :password "12345"
                :user-id (uuid/random-uuid)}
          user-repo (stub-protocol
                      users/UserRepository
                      (-get-user [this user-id] user)) ]
      (is (= user (users/get-user {:hello-service.core/user-repository user-repo}
                                  (:user-id user))))
      (is (invoked? user-repo
                    :method '-get-user
                    :args {'this user-repo
                           'user-id (:user-id user)})))))
