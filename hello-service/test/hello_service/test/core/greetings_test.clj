(ns hello-service.test.core.greetings-test
  (:require [clojure.test :refer :all]
            [fhofherr.stub-fn.clojure.test :refer [stub-protocol]]
            [hello-service.core
             [errors :as errors]
             [greetings :as greetings]
             [uuid :as uuid]
             [users :as users]]))

(def test-user {:user-id (uuid/random-uuid)
                :user-name "John Doe"
                :password "12345"})

(defn- stub-user-repo
  [u]
  (stub-protocol
    users/UserRepository
    (-get-user [this user-id] u)))

(defn- stub-greeting-repo
  [custom-greeting]
  (stub-protocol
    greetings/GreetingRepository
    (-get-greeting [this user-id] custom-greeting)
    (-put-greeting [this user greeting])))

(deftest greeting-users

  (testing "generate a default greeting for an user"
    (let [user-repo (stub-user-repo test-user)
          greeting-repo (stub-greeting-repo nil)
          greeting-message (greetings/greet-user {:hello-service.core/user-repository user-repo
                                                  :hello-service.core/greeting-repository greeting-repo}
                                                 (:user-id test-user))]
      (is (= {:greeting-message "Hello, John Doe"}
             greeting-message))
      (is (invoked? user-repo
                    :method '-get-user
                    :args {'this user-repo
                           'user-id (:user-id test-user)}))
      (is (invoked? greeting-repo
                    :method '-get-greeting
                    :args {'this greeting-repo
                           'user-id (:user-id test-user)}))))

  (testing "use a custom greeting"
    (let [user-repo (stub-user-repo test-user)
          greeting-repo (stub-greeting-repo {:greeting "Ave"
                                             :greeting-id (uuid/random-uuid)})
          greeting-message (greetings/greet-user {:hello-service.core/user-repository user-repo
                                                  :hello-service.core/greeting-repository greeting-repo}
                                                 (:user-id test-user))]
      (is (= {:greeting-message "Ave, John Doe"}))))

  (testing "no greeting for missing user"
    (let [user-repo (stub-user-repo nil)
          greeting-repo (stub-greeting-repo nil)
          greeting-message (greetings/greet-user {:hello-service.core/user-repository user-repo
                                                  :hello-service.core/greeting-repository greeting-repo}
                                                 (:user-id test-user))]
      (is (nil? greeting-message))
      (is (invoked? greeting-repo
                    :method '-get-greeting
                    :times 0)))))

(deftest customizing-greetings

  (testing "set greeting for user"
    (let [user-repo (stub-user-repo test-user)
          greeting-repo (stub-greeting-repo nil)
          greeting (greetings/customize-greeting {:hello-service.core/user-repository user-repo
                                                  :hello-service.core/greeting-repository greeting-repo}
                                                 (:user-id test-user)
                                                 {:greeting "Ave"})]
      (is (= "Ave" (:greeting greeting)))
      (is (not= ::absent (:greeting-id greeting ::absent)))
      (is (invoked? greeting-repo
                    :method '-put-greeting
                    :args {'this greeting-repo
                           'user test-user
                           'greeting greeting}))))

  (testing "set greeting for missing user"
    (let [user-repo (stub-user-repo nil)
          greeting-repo (stub-greeting-repo nil)]
      ;; We don't use thrown? here, as we want to make further assertions
      ;; about the exception thrown.
      ;; TODO: extend assert-expr to make this possible
      (try
        (greetings/customize-greeting {:hello-service.core/user-repository user-repo
                                       :hello-service.core/greeting-repository greeting-repo}
                                      (:user-id test-user)
                                      {:greeting "Ave"})
        (is false "Expected exception to be thrown")
        (catch clojure.lang.ExceptionInfo e
          (let [{:keys [cause detail]} (ex-data e)]
            (is (= ::errors/no-such-user cause))
            (is (not-empty detail)))
          (is (invoked? greeting-repo :method '-put-greeting :times 0)))))))
