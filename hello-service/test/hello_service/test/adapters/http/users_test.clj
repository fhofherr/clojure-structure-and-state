(ns hello-service.test.adapters.http.users-test
  (:require [clojure.test :refer :all]
            [ring.mock.request :as mock]
            [fhofherr.stub-fn.clojure.test :refer [stub-protocol]]
            [hello-service.core.uuid :as uuid]
            [hello-service.adapters.http.users :as users]))

(deftest adding-new-users

  (testing "add a new user"
    (let [ops (stub-protocol
                users/UserOps
                (-add-user [ops new-user]))
          handler (users/new-user-routes ops)
          new-user {:user-name "John Doe" :password "12345"}
          response  (handler (-> (mock/request :post "/")
                                 (assoc :body new-user)))]
      (is (= 201 (:status response)))
      (is (not-empty (get-in response [:headers "Location"])))
      (is (invoked? ops
                    :method '-add-user
                    :args {'ops ops 'new-user new-user}))))

  (testing "get an existing user"
    (let [user {:user-id (uuid/random-uuid)
                :user-name "John Doe"
                :password "12345"}
          ops (stub-protocol
                users/UserOps
                (-get-user [ops user-id] user))
          handler (users/new-user-routes ops)
          response  (handler (mock/request :get (str "/" (:user-id user))))]
      (is (= 200 (:status response)))
      (is (= user (:body response)))
      (is (invoked? ops
                    :method '-get-user
                    :args {'ops ops 'user-id (:user-id user)})))))
