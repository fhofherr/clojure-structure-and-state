(ns hello-service.test.adapters.persistence.support
  (:require [hello-service.adapters.persistence.db :as db]))

(def ^:private db-url "jdbc:h2:mem:test;DB_CLOSE_DELAY=-1")
(def ^:private db-user "")
(def ^:private db-password "")

(defmacro with-test-db
  "Creates a new database and binds it to sym before executing body."
  [sym & body]
  `(let [~sym (db/new-db-spec ~db-url ~db-user ~db-password)]
     (try
       ~@body
       (finally
         (db/close ~sym)))))
