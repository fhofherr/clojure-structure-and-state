(ns hello-service.test.adapters.persistence.users-test
  (:require [clojure.test :refer :all]
            [hello-service.test.adapters.persistence.support :refer [with-test-db]]
            [hello-service.core
             [errors :as errors]
             [users :as users]
             [uuid :as uuid]]
            [hello-service.adapters.persistence.users :refer [new-user-repository]]))

(deftest saving-users-to-the-database

  (testing "add a new user"
    (with-test-db db
      (let [user-repo (new-user-repository db)
            user {:user-id (uuid/random-uuid)
                  :user-name "John Doe"
                  :password "12345"}]
        (users/-put-user user-repo user)
        (is (= user
               (users/-get-user user-repo (:user-id user)))))))

  (testing "add an invalid user"
    (with-test-db db
      (let [user-repo (new-user-repository db)
            user {:user-name "John Doe"
                  :password "12345"}]
        (try
          (users/-put-user user-repo user)
          (is false "Expected exception to be thrown")
          (catch clojure.lang.ExceptionInfo e
            (let [{:keys [cause detail]} (ex-data e)]
              (is (= ::errors/validation-failed cause))
              (is (not-empty detail)))))))))
