(in-ns 'user)
(require '[org.httpkit.client :as http]
         '[cheshire.core :as json])

(refresh-all)
(run-tests)
(start-system)
(stop-system)
(refresh-system)


(def post-user-response (-> "http://localhost:8080/users"
                            (http/post {:headers {"Accept" "application/json"
                                                  "Content-Type" "application/json"}
                                         :body (json/generate-string {:user-name "John Doe"
                                                                 :password "12345"})})
                            deref
                            (update :body json/parse-string true)))

(pprint post-user-response)

(def user-id (get-in post-user-response [:body :user-id]))

(pprint user-id)

(def get-user-response (-> "http://localhost:8080/users"
                               (str "/" user-id)
                               (http/get {:headers {"Accept" "application/json"}})
                               deref
                               (update :body json/parse-string)))

(pprint get-user-response)

;; TODO I did not get to that yet :-(
(def get-greeting-response (-> "http://localhost:8080/greetings"
                               (str "/" user-id)
                               (http/get {:headers {"Accept" "application/json"}})
                               deref
                               #_(update :body json/parse-string)))

(pprint get-greeting-response)
