(ns user
  (:require [clojure.java.io :as io]
            [clojure.string :as str]
            [clojure.pprint :refer (pprint)]
            [clojure.repl :refer :all]
            [clojure.test :as t]
            [clojure.tools.namespace.repl :as repl]
            [hello-service.main :as main]))

(def system {})

(defn start-system
  []
  (alter-var-root #'system main/init)
  (println "System started"))

(defn stop-system
  []
  (when-not (empty? system)
    (alter-var-root #'system main/shutdown))
  (println "System stopped"))

(defn refresh
  [& {:keys [after]}]
  (when (not-empty system)
    (stop-system))
  (if after
    (repl/refresh :after after)
    (repl/refresh)))

(defn refresh-all
  [& {:keys [after]}]
  (stop-system)
  (if after
    (repl/refresh-all :after after)
    (repl/refresh-all)))

(defn refresh-system
  []
  (refresh-all :after 'user/start-system))

(defn- do-test
  []
  (t/run-all-tests #"hello-service\.test\..+-test"))

(defn run-tests
  []
  (refresh :after 'user/do-test))
